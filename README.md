# filedrop
File drop is a file upload site made in SimpleScript and using the NodeHost Storage Platform for storage.

So what you need to do to use is install SimpleScript as you can find at https://www.codesimplescript.com and place the files in this project into the /www folder SimpleScript will create. Next just open `settings.ssc` and enter your API key and change any other settings you want. Replace the folder ID with the ID of a folder you create on the NodeHost Storage Platform. You can find the ID after you make the folder by clicking API URL and it will contain a string like `api.nodehost.ca/apikey/storageplatform_create_file/?folder=68` making your folder ID 68.
